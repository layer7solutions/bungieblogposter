from configparser import SafeConfigParser
from datetime import datetime, timedelta

import requests


class ImgurAccessToken:
    def __init__(self, logger, path_to_cfg):
        self.logger = logger
        self.path_to_cfg = path_to_cfg
        self.cfg_file = SafeConfigParser()
        self.cfg_file.read(self.path_to_cfg)

        self.OAuthTokenURL = "https://api.imgur.com/oauth2/token"
        self.ClientID = self.cfg_file.get("imgur", "clientid")
        self.ClientSecret = self.cfg_file.get("imgur", "clientsecret")
        self.RefreshToken = self.cfg_file.get("imgur", "refreshtoken")
        self.AccessTokenExpiry = datetime.now()
        self.AccessToken = "INVALID"

    @property
    def Token(self):
        if self.AccessTokenExpiry > datetime.now():
            return self.AccessToken

        # Access token expires after 1 day, we'll always need a new one
        self.logger.debug("access token expired, getting new")
        r = requests.post(
            self.OAuthTokenURL,
            data={
                "grant_type": "refresh_token",
                "client_id": self.ClientID,
                "client_secret": self.ClientSecret,
                "refresh_token": self.RefreshToken,
            },
        )
        if r.status_code != 200:
            raise Exception("OAuth RefreshToken request failed: {}".format(r.text))

        json = r.json()
        self.AccessToken = json["access_token"]
        self.AccessTokenExpiry = datetime.now() + timedelta(seconds=int(json["expires_in"]))
        self.RefreshToken = json["refresh_token"]

        # save the new refreshtoken
        self.cfg_file.set("imgur", "refreshtoken", self.RefreshToken)
        self.cfg_file.write(open(self.path_to_cfg, "w"))

        return self.AccessToken


class ImgurAPI:
    def __init__(self, logger, path_to_cfg):
        self.logger = logger

        cfg_file = SafeConfigParser()
        cfg_file.read(path_to_cfg)
        try:
            self.AccountName = cfg_file.get("imgur", "accountname")
            self.AccountID = cfg_file.get("imgur", "accountid")

            self.AccessToken = ImgurAccessToken(logger, path_to_cfg)
            self.Available = True
        except Exception:
            self.Available = False
            return

        self.BaseURL = "https://api.imgur.com/3"

    @property
    def Headers(self):
        return {
            "Accept": "application/vnd.api+json",
            "Authorization": "Bearer {}".format(self.AccessToken.Token),
        }

    def albumCreate(self, title="", description="", privacy="", cover="", ids=[]):
        if not self.Available:
            return False

        self.logger.debug("API: called albumCreate")
        payload = {
            "title": title,
            "description": description,
            "privacy": privacy,
            "cover": cover,
        }
        if ids:
            payload["ids"]: ",".join(ids)
        r = requests.post("{}/album".format(self.BaseURL), headers=self.Headers, data=payload)
        if r.status_code != 200:
            raise RuntimeError("Request failed: {}".format(r.text))
        return r.json()

    def albumAddImgs(self, albumHash, ids=[]):
        if not self.Available:
            return False

        self.logger.debug("API: called albumAddImgs")
        payload = {"ids": ",".join(ids)}
        r = requests.post("{}/album/{}/add".format(self.BaseURL, albumHash), headers=self.Headers, data=payload,)
        if r.status_code != 200:
            raise RuntimeError("Request failed: {}".format(r.text))
        return r.json()

    def imageUpload(self, image, album="", imgType="", name="", title="", description=""):
        if not self.Available:
            return False

        self.logger.debug("API: called imageUpload")
        payload = {
            "image": image,
            "album": album,
            "type": imgType,
            "name": name,
            "title": title,
            "description": description,
        }
        r = requests.post("{}/image".format(self.BaseURL), headers=self.Headers, data=payload)
        if r.status_code != 200:
            raise RuntimeError("Request failed: {}".format(r.text))
        return r.json()

    def imageUpdate(self, imageHash, title="", description=""):
        if not self.Available:
            return False

        self.logger.debug("API: called imageUpdate")
        payload = {"title": title, "description": description}
        r = requests.post("{}/image/{}".format(self.BaseURL, imageHash), headers=self.Headers, data=payload,)
        if r.status_code != 200:
            raise RuntimeError("Request failed: {}".format(r.text))
        return r.json()

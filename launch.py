import configparser
import json
import logging
import logging.config
import re
import sys
import time
from datetime import datetime, timezone

import bs4
import feedparser
import praw
import prawcore
import psycopg2
import requests
import urllib3
from layer7_utilities import LoggerConfig, oAuth
from requests.exceptions import Timeout

from _version import __version__
from ImgurAPI import ImgurAPI

botconfig = configparser.ConfigParser()
botconfig.read("botconfig.ini")

__botname__ = "Bungie Blog Poster"
__description__ = "Posts blog posts from Bungie.net to Reddit."
__author__ = "u/D0cR3d & u/Try_to_guess_my_psn"
__dsn__ = botconfig.get("BotConfig", "DSN")
__logpath__ = botconfig.get("BotConfig", "logpath")
API_KEY = botconfig.get("BungieAPI", "Key")

# ~~~~~~~~~~ Import Config Settings ~~~~~~~~~~ #
DB_USERNAME = botconfig.get("Database", "Username")
DB_PASSWORD = botconfig.get("Database", "Password")
DatabaseName = botconfig.get("Database", "DatabaseName")
DB_HOST = botconfig.get("Database", "Host")
# ~~~~~~~~~~ Import Config Settings ~~~~~~~~~~ #

# Production
SUBREDDIT = "DestinyTheGame"
OTHERSUBS = [{"name": "LowSodiumDestiny", "flair": "News"}]
BungieBlogURL = "https://www.bungie.net/en/rss/news?includebody=true&currentpage=0"
NotificationsEnabled = True
CollectionList = {
    "_default": "cbb60838-e0e8-4ce4-bd86-7fa90dbe1bea",
    "This Week At Bungie": "3c574ed8-cf33-4caa-a029-e933c9bc4a71",
    "Destiny Update": "616ab790-3d73-493d-96e9-608fceffe40a",
    "Destiny 2 Update": "616ab790-3d73-493d-96e9-608fceffe40a",
    "Destiny Hotfix": "616ab790-3d73-493d-96e9-608fceffe40a",
    "Destiny 2 Hotfix": "616ab790-3d73-493d-96e9-608fceffe40a",
}

"""
# Development
SUBREDDIT = "ttgmbot"
OTHERSUBS = []
BungieBlogURL = "https://www.bungie.net/en/rss/news?includebody=true&currentpage=0"
NotificationsEnabled = False
CollectionList = {
    "_default": "b3c3af84-d8b7-4a4b-b973-a20ff668b64f",
    "This Week At Bungie": "8e79297c-1af8-485a-9be4-360756c62e20",
    "Destiny Update": "013ba32b-88d8-4e65-91b4-fb32a4fd109b",
    "Destiny 2 Update": "013ba32b-88d8-4e65-91b4-fb32a4fd109b",
    "Destiny Hotfix": "013ba32b-88d8-4e65-91b4-fb32a4fd109b",
    "Destiny 2 Hotfix": "013ba32b-88d8-4e65-91b4-fb32a4fd109b",
}
"""

# possible attrs: ReplaceStart, ReplaceEnd, CanBeEmpty, IsImage, IsVideo, IsLink, IsQuote, IsListStart, IsListElement, NewLineBefore, NewLineAfter, RemoveSpace, NoNewLine
translate = {
    "i": {
        "replace": "*",
        "attrs": ["ReplaceStart", "ReplaceEnd", "RemoveSpace", "NoNewLine"],
    },
    "em": {
        "replace": "*",
        "attrs": ["ReplaceStart", "ReplaceEnd", "RemoveSpace", "NoNewLine"],
    },
    "b": {
        "replace": "**",
        "attrs": ["ReplaceStart", "ReplaceEnd", "RemoveSpace", "NoNewLine"],
    },
    "strong": {
        "replace": "**",
        "attrs": ["ReplaceStart", "ReplaceEnd", "RemoveSpace", "NoNewLine"],
    },
    "s": {"replace": "~~", "attrs": ["ReplaceStart", "ReplaceEnd", "RemoveSpace"]},
    "br": {"replace": "\n\n", "attrs": ["ReplaceStart", "CanBeEmpty"]},
    "p": {"replace": "\n\n", "attrs": ["ReplaceStart", "ReplaceEnd"]},
    "hr": {
        "replace": "\n***\n",
        "attrs": ["ReplaceStart", "CanBeEmpty", "NewLineBefore", "NewLineAfter"],
    },
    "h1": {"replace": "#", "attrs": ["ReplaceStart", "NewLineBefore", "NewLineAfter"]},
    "h2": {"replace": "##", "attrs": ["ReplaceStart", "NewLineBefore", "NewLineAfter"]},
    "h3": {
        "replace": "###",
        "attrs": ["ReplaceStart", "NewLineBefore", "NewLineAfter"],
    },
    "h4": {
        "replace": "####",
        "attrs": ["ReplaceStart", "NewLineBefore", "NewLineAfter"],
    },
    "ul": {"replace": "", "attrs": ["IsListStart", "NewLineBefore"]},
    "ol": {"replace": "", "attrs": ["IsListStart", "IsNumberedList", "NewLineBefore"]},
    "li": {"replace": "", "attrs": ["IsListElement", "NewLineBefore"]},
    "a": {"replace": "", "attrs": ["IsLink"]},
    "div": {"replace": "", "attrs": ["NewLineBefore", "NewLineAfter"]},
    "figure": {"replace": "", "attrs": ["NewLineBefore", "NewLineAfter"]},  # treat it like a div (a container)
    "img": {
        "replace": "\n",
        "attrs": ["IsImage", "NewLineBefore", "ReplaceStart", "CanBeEmpty"],
    },
    "iframe": {
        "replace": "",
        "attrs": ["IsVideo", "CanBeEmpty"],
    },  # no additional \n for Videos, looks better in the MOTW section :)
    "video": {
        "replace": "\n",
        "attrs": ["IsVideo", "NewLineBefore", "ReplaceStart", "CanBeEmpty"],
    },
    "blockquote": {"replace": "", "attrs": ["IsQuote", "NewLineBefore"]},
    "table": {"replace": "", "attrs": ["IsTable", "NewLineBefore", "NoNewLine"]},
    "tr": {"replace": "", "attrs": ["IsTr"]},
    "td": {"replace": "", "attrs": ["IsTd", "CanBeEmpty"]},
    "code": {"replace": "", "attrs": ["IsCode", "NewLineBefore", "NewLineAfter"]},
    # ignored tags below
    "u": {"replace": "", "attrs": []},
    "big": {"replace": "", "attrs": []},
    "small": {"replace": "", "attrs": []},
    "span": {"replace": "", "attrs": []},
    "tbody": {"replace": "", "attrs": []},
    "script": {"replace": "", "attrs": []},
    "strike": {"replace": "", "attrs": []},
    "pre": {"replace": "", "attrs": []},
}

notification_channels = {
    "Bungie_Discord": "https://discordapp.com/api/webhooks/632033436990832643/YW5hQlCCc66fp5Zu76w1mOUW-BcC7vZV4krA0lJHJC3rOcBVSedqfprctqY6P0i5BPj3/slack",  # Literally the official Bungie (Destiny) Discord ServerID 595735685239930989
    "DTG_Slack": "https://hooks.slack.com/services/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",
    "DTG_Discord": "https://discordapp.com/api/webhooks/289554414551695361/mJ6Vj8Y4sep9J6FxVurP3gY5xX9P3ulbEaV-NVXFoicf8KFYOTVs_EdLkBXjP7xOqZOW/slack",
    "TMT_Discord": "https://discordapp.com/api/webhooks/289553329934041088/cRHCsSrpkgrlMjGR308hIZBhOkGaw9FJ-XZOAElcLQy7Mmp8DfsBfdYlzIEQWaCBcPOo/slack",
    "ChrisG_Discord": "https://discordapp.com/api/webhooks/289554623298011137/b8lauNMct735S_YE7UkbarieRMkWz1Wv6copTcIGuL20Il3nJ2a4ZG5F0WdLgmpisMdi/slack",
    "BitesizedPwr_Discord": "https://discordapp.com/api/webhooks/289557301181022208/A8Rn2dmHDCmpUBmbWutih_1bdra_WimiJU_wUgTOCzMivjB_5TsTdJyUaT9nwjdjKNvp/slack",
    "Justine_Discord": "https://discordapp.com/api/webhooks/297333209409126400/JM6ZVMytNHdxsgiqLF7-PLcbadM1QtYhZlDc5nN0O0qaDygJ4RDxHNNE0zuMgCxJwhzo/slack",
    "Ramblinnn_Discord": "https://discordapp.com/api/webhooks/347363602950389761/cCjPrxK4rzA2ZXLng4n7dtty3XLhTGaykCKi337NfDw5COFRaAtD5B88VlQfRgzbU8rA/slack",
    "LotZor#8231_Discord": "https://discordapp.com/api/webhooks/352983216241770497/549qQi6h4uF_AhFGnlV6K2bmnG8aIzqjU72MhZcgYBR732U1RsJFQn2JafSBpciQNokL/slack",
    "fightn-ares#5530_Discord": "https://discordapp.com/api/webhooks/355117788731539458/5hk_Fw6c2oGhUWG9lmmj3WXcrD4b78z3-TTn6xvZ0Xm_BxVoW1RGDY55g2t2SRG088DU/slack",
    "Smebbs_Discord": "https://discordapp.com/api/webhooks/360802564532338688/rYywpP23329RMS2Ur6MJ7DWF6Ggl3XfxIQr6V9k1HtbE1Uqvk2kNwmjjxzEcTStFIRQ8/slack",
    "AndSoItBegins_Discord": "https://discordapp.com/api/webhooks/500777518467383308/MCK-slX8QLdVbYkdt2ebccjIbHh8v_Ff7BEBiDbe_tzaDeTSrJ6kLIWrWHADuIgQw6Of/slack",
    "AESTHETHICC_Discord": "https://discordapp.com/api/webhooks/500785211269840928/yxa51iEQmJGL6GMnSDcnPIdYcT5iV80Nd6Z7U_Rc7nxNM1THrBQ2RdgAfxOMsz4pzWYT/slack",
    "DTG_Clan_Discord": "https://discordapp.com/api/webhooks/500785500320170017/eaJmdPBETsU3huvseeuVhDqn4Z6uGC4CZ4bZAqPinvd220tGz9gMa3JH0_zncjCv7IRe/slack",
    "Justine_Seth_Discord": "https://discordapp.com/api/webhooks/530526928785113089/WnHKyCOfwU4cwUF2a1cUJTsjL7GY3RN2klnnciQTMZ8ZlyAdkeTAtsClrjQqQwc1pojh/slack",
    "Cryo#8291_Discord": "https://discordapp.com/api/webhooks/611708765552640000/DszOaD-E3hie6qrVBhp49KueI7EE8KXF7qreOUxaYCgR_YjhkrRgTyoRUrjvC1pBgW8S/slack",  # UserID 154289231861252107
    "Support#9288_Discord": "https://discord.com/api/webhooks/889961935003607080/4KqPS19Mxg58vslypyPr93cQI0tqriK2tBf_PafZMmVumVzX4FFOTMrJa8kSNEo5uopg/slack",  # UserID 627503122683658250
    "Seth_Discord": "https://discord.com/api/webhooks/939959487190687776/ddS9X01mYjqySQANDcuMKMlZc8l671WysrccTFoVDGoOW9ltIPw_auA_ONuCgW6PGjjh/slack",  # UserID 122932725895135234
}

FLAG_QUOTE = pow(2, 0)
FLAG_NEWLINES = pow(2, 1)
FLAG_TABLE = pow(2, 2)
FLAG_TABLEHEADER = pow(2, 3)
FLAG_CODE = pow(2, 4)
FLAG_NUMBEREDLIST = pow(2, 5)


class BungieRequest:
    def __init__(self, logger):
        self.logger = logger
        self.last_request = None
        self.this_sec_req_count = 0

    # request_ratelimit
    def request_ratelimit(self, req_per_sec, backoff_time=5):
        """
        request_ratelimit

        Allows throttling/rate-limiting of requests per second --
        the function will use global variables in global state to
        determine how many requests were made this second and,
        if it's more than req_per_sec, will time.sleep(backoff_time).

        (In case that's not clear: this is not thread-safe!)
        """
        if not self.last_request or self.last_request < time.time():
            self.last_request = time.time()
            self.this_sec_req_count = 0
        else:
            self.logger.debug("rate limiting...")
            self.this_sec_req_count += 1
            if self.this_sec_req_count >= req_per_sec:
                time.sleep(backoff_time)
                self.last_request = time.time()
                self.this_sec_req_count = 0

    # bungie_request
    def bungie_request(self, URI):
        """
        bungie_request

        Makes a request to Bungie's API at URI (always prefixed with /)

        Returns None if the HTTP request failed, False if the API returned
        an error, and the Response part of the JSON data returned by the
        API
        """

        # Rate-limit
        self.request_ratelimit(10, 2)

        headers = {"X-API-Key": API_KEY}
        try:
            req = requests.get("https://www.bungie.net{0}".format(URI), headers=headers)
        except Exception:
            self.logger.warning("Exception from left field")
            return None

        result = req.json()
        if result["ErrorCode"] != 1:
            return False
        else:
            return result["Response"]

    def get_bungie_blog_post_html(self, post_id):
        response = self.get_bungie_blog_post(post_id)
        if response:
            if response["cType"] == "News":
                return response["properties"]["Content"]
            elif response["cType"] == "StaticAsset":
                # XXX: emit HTML even though it's technically just
                # XXX: some static asset link... the CMS probably
                # XXX: does similar autodetect.
                if response["cmsPath"].startswith("watch?"):
                    return '<iframe src="{0}"></iframe>'.format(response["properties"]["Path"])
                else:
                    return '<img src="https://www.bungie.net{0}"></img>'.format(response["properties"]["Path"])
            else:
                self.logger.error("Unknown cType {}".format(response["cType"]))
        else:
            return False

    def get_bungie_blog_post(self, post_id):
        return self.bungie_request(
            "/Platform/Content/GetContentById/{0}/en/?lc=en&fmt=true&lcin=true&head=false".format(post_id)
        )


class MarkdownEmitter:
    def __init__(self, logger, request, imgur):
        self.logger = logger
        self.request = request
        self.imgur = imgur
        self.output = ""
        self.newlinebuffer = ""
        self.bulletlevel = 0
        self.replaceSpaceIfStartsWithOne = False

        self.imgurUploads = []

    def get(self, html):
        html = html.replace("<div>&nbsp;</div>", "<div><br></div>")
        self.parse(bs4.BeautifulSoup(html, "html.parser"))
        return re.sub("(?m)^\n(\n\n+)$", "\n", self.output).strip()

    def parse(self, tag, flags=FLAG_NEWLINES):
        for child in tag.children:
            if type(child) is bs4.element.Tag:
                try:
                    tag = child.name
                    if tag not in translate:
                        self.logger.warn("Encountered unknown tag '{}'".format(child.name))
                        replace = ""
                        attrs = []
                    else:
                        replace = translate[tag]["replace"]
                        attrs = translate[tag]["attrs"]
                        if not (flags & FLAG_NEWLINES) == FLAG_NEWLINES and "\n" in replace:
                            for _ in range(replace.count("\n")):
                                self.newlinebuffer += "\n"
                            replace = replace.replace("\n", "")

                    # handle child['src'] & child['href']
                    if child.has_attr("src") and child["src"][0] == "/":
                        child["src"] = "https://www.bungie.net" + child["src"]
                    if child.has_attr("href") and child["href"][0] == "/":
                        child["href"] = "https://www.bungie.net" + child["href"]

                    isNotEmpty = len(list(child.stripped_strings)) > 0 or "CanBeEmpty" in attrs

                    # HANDLE TAG START
                    if "NoNewLine" in attrs:
                        flags = flags & ~FLAG_NEWLINES
                    newoutput = ""
                    if "NewLineBefore" in attrs and (flags & FLAG_NEWLINES) == FLAG_NEWLINES:
                        newoutput += "\n"
                    if "IsListStart" in attrs:
                        if "IsNumberedList" in attrs:
                            flags = flags | FLAG_NUMBEREDLIST
                        listContainsElements = False
                        for le in child.children:
                            if (
                                type(le) is bs4.element.Tag
                                and le.name in translate
                                and "IsListElement" in translate[le.name]["attrs"]
                            ):
                                listContainsElements = True
                        if listContainsElements:
                            self.bulletlevel += 1
                    if "IsListElement" in attrs:
                        for _ in range(self.bulletlevel - 1):
                            newoutput += "   "
                        if (flags & FLAG_NUMBEREDLIST) == FLAG_NUMBEREDLIST:
                            newoutput += "1. "
                        else:
                            newoutput += "* "
                    if "IsQuote" in attrs:
                        flags = flags | FLAG_QUOTE
                    if "IsCode" in attrs:
                        flags = flags | FLAG_CODE
                    if "ReplaceStart" in attrs and isNotEmpty:
                        newoutput += replace
                    if (flags & FLAG_QUOTE) == FLAG_QUOTE:
                        newoutput = newoutput.replace("\n", "\n>")
                    if (flags & FLAG_CODE) == FLAG_CODE:
                        newoutput += "\n    "
                    self.output += newoutput
                    if "RemoveSpace" in attrs:
                        self.replaceSpaceIfStartsWithOne = True

                    # HANDLE TAG START SPECIAL
                    if "IsLink" in attrs:
                        self.output += "["
                    if "IsImage" in attrs and child.has_attr("src"):
                        if child["src"][0:4] != "data":
                            self.output += "[Image Link]({})".format(child["src"])
                        # imgur upload
                        if self.imgur.Available:
                            try:
                                imgPrefix = ""
                                if child["src"][0:4] != "data":
                                    resp = self.imgur.imageUpload(
                                        image=child["src"],
                                        imgType="URL",
                                        description="Image Source: {}".format(child["src"]),
                                    )
                                    imgPrefix = "^^^"
                                else:
                                    resp = self.imgur.imageUpload(image=child["src"][22:], imgType="base64")
                                if resp["success"] and resp["status"] == 200:
                                    self.imgurUploads.append(str(resp["data"]["id"]))
                                self.output += "{}[imgur]({})".format(imgPrefix, resp["data"]["link"])
                            except RuntimeError as e:
                                self.logger.exception("Can't upload image to imgur: {}".format(e))
                    if "IsVideo" in attrs and child.has_attr("src"):
                        self.output += "[Video Link]({})".format(child["src"])
                        if self.imgur.Available and child["src"].endswith(".mp4"):
                            resp = self.imgur.imageUpload(
                                image=child["src"],
                                imgType="URL",
                                description="Video Source: {}".format(child["src"]),
                            )
                            if resp["success"] and resp["status"] == 200:
                                self.imgurUploads.append(str(resp["data"]["id"]))
                    if "IsTable" in attrs:
                        flags = flags | FLAG_TABLE
                        flags = flags | FLAG_TABLEHEADER
                    if "IsTr" in attrs:
                        self.output += "\n"
                        if flags & FLAG_QUOTE:
                            self.output += ">"

                    # PARSE SUBTAGS
                    self.parse(child, flags)

                    # HANDLE TAG END SPECIAL
                    if "IsLink" in attrs:
                        if child.has_attr("href"):
                            self.output += "]({})".format(child["href"])
                        else:
                            self.output += "](#missinglink)"
                    if "IsQuote" in attrs:
                        flags = flags & ~FLAG_QUOTE
                        if self.output[-3:] == ">\n>":
                            self.output = self.output[:-3] + "\n\n"
                        elif self.output[-2:] == "\n>":
                            self.output = self.output[:-2] + "\n\n"
                    if "IsCode" in attrs:
                        flags = flags & ~FLAG_CODE
                    if "IsTd" in attrs:
                        self.output += "|"
                    if "IsTr" in attrs:
                        if flags & FLAG_TABLEHEADER:
                            # exact amount doesn't matter, just has to be more than columns
                            self.output += "\n"
                            if flags & FLAG_QUOTE:
                                self.output += ">"
                            self.output += "|--|--|--|--|--|--|--|--|--|"
                            flags = flags & ~FLAG_TABLEHEADER
                    if "IsTable" in attrs:
                        flags = flags & ~FLAG_TABLE
                        flags = flags & ~FLAG_TABLEHEADER

                    # HANDLE TAG END
                    self.replaceSpaceIfStartsWithOne = False
                    newoutput = ""
                    if "ReplaceEnd" in attrs and isNotEmpty:
                        appendSpace = False
                        if "RemoveSpace" in attrs and self.output[-1:] == " ":
                            appendSpace = True
                            self.output = self.output[:-1]
                        newoutput += replace
                        if appendSpace:
                            newoutput += " "
                    if "IsListStart" in attrs:
                        if listContainsElements:
                            self.bulletlevel -= 1
                        if self.bulletlevel == 0 and "IsNumberedList" in attrs:
                            flags = flags & ~FLAG_NUMBEREDLIST
                    if "NoNewLine" in attrs:
                        flags = flags | FLAG_NEWLINES
                        newoutput += self.newlinebuffer
                        self.newlinebuffer = ""
                    if "NewLineAfter" in attrs and (flags & FLAG_NEWLINES) == FLAG_NEWLINES:
                        newoutput += "\n"
                    if (flags & FLAG_QUOTE) == FLAG_QUOTE:
                        newoutput = newoutput.replace("\n", "\n>")
                    self.output += newoutput
                except Exception as err:
                    self.logger.exception(f"Something dun fucked with the formatting. Error: {err}")

            if type(child) is bs4.element.NavigableString:
                m = re.match(
                    r"\s*\[\[\s*data-content-id='([0-9]+)'\s+data-template-type='(\w+)'[^\]]*\]\]\s*",
                    child.string,
                )
                if not m is None:
                    if m.groups()[1] != "Inline":
                        self.logger.error("Unknown data-template-type {}".format(m.groups()[1]))
                        continue
                    html = self.request.get_bungie_blog_post_html(m.groups()[0])
                    # replace youtu.be "imgs" because Bungie is too stupid to output them as iframe...
                    html = re.sub(
                        r'<img src="https://www\.bungie\.nethttp([^"]*)"></img>',
                        r'<iframe src="http\1"></iframe>',
                        html,
                    )
                    self.parse(bs4.BeautifulSoup(html, "html.parser"), flags)
                else:
                    s = child.string
                    if self.replaceSpaceIfStartsWithOne and s[0] == " ":
                        s = "&nbsp;" + s[1:]
                    self.replaceSpaceIfStartsWithOne = False
                    if not (flags & FLAG_NEWLINES) == FLAG_NEWLINES and "\n" in s:
                        for _ in range(s.count("\n")):
                            self.newlinebuffer += "\n"
                        s = s.replace("\n", "")
                    if (flags & FLAG_CODE) == FLAG_CODE and "\n" in s:
                        s = s.replace("\n", "\n    ")
                    self.output += s


class blogbot:
    def __init__(self):
        self.conn = None
        #################### LOGGER SETUP ############################
        # Setup logging
        config = LoggerConfig(__dsn__, __botname__, __version__, __logpath__)
        logging.config.dictConfig(config.get_config())
        self.logger = logging.getLogger("root")
        self.logger.info("/*********Starting App*********\\")
        self.logger.info("App Name: {} | Version: {}".format(__botname__, __version__))
        ################################################################

        self.canLaunch = self.login()
        self.request = BungieRequest(self.logger)

        # Loads in the Database and creates if it doesn't already exist
        self.init_database()
        self.logger.info("Connected to DB")

        self.imgur = ImgurAPI(self.logger, "botconfig.ini")

    def init_database(self):
        if self.conn:
            if not self.conn.closed:
                self.conn.close()

        self.conn = psycopg2.connect(
            host=DB_HOST,
            dbname=DatabaseName,
            user=DB_USERNAME,
            password=DB_PASSWORD,
            keepalives=1,
            keepalives_idle=30,
            keepalives_interval=10,
            keepalives_count=5,
        )
        self.conn.autocommit = True

    def login(self):
        try:
            self.logger.debug("/*********Getting Accounts*********\\")
            auth = oAuth()
            auth.get_accounts(
                "dtgbot",
                __description__,
                __version__,
                __author__,
                __botname__,
                DB_USERNAME,
                DB_PASSWORD,
                DB_HOST,
                "TheTraveler",
            )
            for account in auth.accounts:
                self.r = account.login()
                break
            self.logger.info("Connected to account: {}".format(self.r.user.me()))
            return True
        except Exception:
            self.logger.exception("Failed to log in.")
            return False

    def notifier(self, pubdate, blogtitle, bloglink, postid):
        headers = {"content-type": "application/json"}

        for server, url in notification_channels.items():
            if server == "Bungie_Discord":
                payload = {
                    "username": "Bungie",
                    "text": f"**New Bungie Blog Post:** {blogtitle}\n**Bungie Link:** {bloglink}",
                }
                requests.post(url, data=json.dumps(payload), headers=headers).text
                self.logger.info(f"Alerted in {server}")
            # Slack
            elif server.endswith("Slack"):
                payload = {
                    "username": "Tower Radio",
                    "text": f"*New Bungie Post:* [{pubdate}]: {blogtitle}\n*Bungie Link:* {bloglink} \n*Reddit Link:* http://redd.it/{postid if postid else ''}",
                }
                requests.post(url, data=json.dumps(payload), headers=headers)
                self.logger.info(f"Alerted in {server}")
            # Discord
            elif server.endswith("Discord"):
                payload = {
                    "username": "Tower Radio",
                    "text": f"**New Bungie Post:** [{pubdate}]: {blogtitle}\n**Bungie Link:** {bloglink} \n**Reddit Link:** http://redd.it/{postid if postid else ''}",
                }
                requests.post(url, data=json.dumps(payload), headers=headers).text
                self.logger.info(f"Alerted in {server}")
            else:
                pass

    def checkDMs(self):
        for msg in self.r.inbox.unread(limit=50):
            # Only handle messages (DMs)
            if not isinstance(msg, praw.models.reddit.message.Message):
                continue
            if not msg.subject == "BungieBlog":
                continue
            if not msg.author in self.r.subreddit(SUBREDDIT).moderator():
                self.logger.warning("[DM] Got DM from non-mod {}".format(msg.author))
                continue
            msg.mark_read()
            parts = msg.body.split(" ")
            try:
                submission = self.r.submission(url=parts[0])
            except praw.exceptions.ClientException as e:
                self.logger.exception("[DM] Couldn't get details for submission! {}".format(str(e)))
                msg.reply("Sorry, you provided an invalid URL ({}).".format(parts[0]))
                continue
            if submission.author != self.r.user.me():
                self.logger.warning("[DM] That submission wasn't made by me")
                msg.reply("Sorry, I'm not the author of the post {}.".format(parts[0]))
                continue

            m = re.match(
                r"^Source: (https://www\.bungie\.net(/|/7/)en/news/article/(.+))\n", submission.selftext, re.IGNORECASE
            )
            if m is None:
                self.logger.warning("[DM] That submission doesn't look like a TWAB Reddit post")
                msg.reply("Sorry, the post {} doesn't look like a Bungie Blog post on Reddit".format(parts[0]))
                continue

            try:
                bloglink = m.groups()[0]
                feed: feedparser.FeedParserDict = feedparser.parse(BungieBlogURL)
                found = False
                for blog in feed.get("entries"):
                    if blog.link.lower() == bloglink.lower():
                        found = True
                        x = blog.content[0].value
                        h = MarkdownEmitter(self.logger, self.request, self.imgur)
                        RedditFormattedText = h.get(x)

                        submission.edit(body="Source: " + str(bloglink) + "\n\n---\n\n" + RedditFormattedText)
                        msg.reply("Success! I've updated {} with the text from {}.".format(parts[0], bloglink))
                        break
                if not found:
                    self.logger.error("[DM] Unable to find blog link %r in the feed", bloglink)
                    msg.reply("Unable to find the blog post in the feed. Please contact a r/layer7!")
            except Exception as err:
                self.logger.exception(f"[DM] Error editing thread: {err}")
                msg.reply("An error occurred editing the thread. Please contact a r/layer7!")

    def main(self):
        while self.canLaunch:
            meta = None
            try:
                self.logger.debug(f"Performing loop check")
                self.checkDMs()
                # Gets the RSS Feed data from Bungie News Blog
                # checks for new Blog Items
                feed: feedparser.FeedParserDict = feedparser.parse(BungieBlogURL)
                for blog in feed.get("entries"):
                    with self.conn.cursor() as c:
                        blogguid = blog.id
                        blogtitle = blog.title.replace("\u2013", "")
                        bloglink = blog.link
                        # The post date object has to be built manually because feedparser provides a time tuple with no time
                        # zone info
                        # time.mktime() assumes current time zone, when we know Bungie posts are GMT
                        blogdate = datetime(
                            year=blog.published_parsed.tm_year,
                            month=blog.published_parsed.tm_mon,
                            day=blog.published_parsed.tm_mday,
                            hour=blog.published_parsed.tm_hour,
                            minute=blog.published_parsed.tm_min,
                            second=blog.published_parsed.tm_sec,
                            tzinfo=timezone.utc,
                        )
                        # HACK: Due to Bungie's CMS migration, we need to ONLY process blog posts past the change!
                        # We can remove this after older posts fall off the front page of the feed, so check around
                        # June 2023 to see.
                        # First post after CMS change: Destiny 2 Update 6.3.0
                        if blogdate < datetime(2022, 12, 6, 0, 0, tzinfo=timezone.utc):
                            self.logger.debug(
                                f"[2022-12 CMS Upgrade] Skipping post [{blogtitle}] because it was before migration"
                            )
                            continue

                        # If it's older than 2025-02-12 when we did improvements to backfill the db
                        if blogdate < datetime(2025, 2, 11, 23, 59, tzinfo=timezone.utc):
                            self.logger.debug(
                                f"Skipping post [{blog.published}] [{blogtitle}] because it was before fixing database issue"
                            )
                            continue

                        # c.execute(f"SELECT bLink FROM bungie_blogs WHERE bLink={bloglink}")
                        c.execute("SELECT bguid FROM bungie_blogs WHERE bguid=%s", [blogguid])
                        fetched = c.fetchone()
                        if not fetched:
                            # Let us know new blog item exists
                            self.logger.info(f"New Bungie Blog: [{blog.published}] | {blogtitle} | {bloglink}")

                            # Goes and gets article text and formats it
                            x = blog.content[0].value
                            h = MarkdownEmitter(self.logger, self.request, self.imgur)
                            RedditFormattedText = h.get(x)

                            # log it in the Database
                            c.execute(
                                "INSERT INTO bungie_blogs (bpubdate, btitle, bdesc, blink, bguid) VALUES (%s, %s, %s, %s, %s)",
                                [blog.published, blogtitle, blog.description, bloglink, blogguid],
                            )

                            # Check for collection
                            collection_id = CollectionList["_default"]
                            for coll_str, coll_id in CollectionList.items():
                                if coll_str.lower() in blogtitle.lower():
                                    collection_id = coll_id
                                    break

                            try:
                                # Limit the string to 39,800 characters. Posts are 40,000, then some buffer
                                PostLen = 39800
                                CommentLen = 9800
                                PostText = RedditFormattedText[:PostLen]
                                CommentText = RedditFormattedText[PostLen:]
                                Comments = [
                                    CommentText[index : index + CommentLen]
                                    for index in range(0, len(CommentText), CommentLen)
                                ]
                                # Create post on DTG and set flair
                                submission = None
                                try:
                                    submission = self.r.subreddit(SUBREDDIT).submit(
                                        blogtitle,
                                        selftext="Source: " + str(bloglink) + "\n\n---\n\n" + PostText,
                                        send_replies=False,
                                        resubmit=None,
                                        collection_id=collection_id,
                                    )
                                    submission.flair.select("2f13ba30-3d46-11e9-8cd7-0e03d8e4b5da")

                                    CurrentParent = submission
                                    FirstComment = True
                                    for Comment in Comments:
                                        CurrentParent = CurrentParent.reply(Comment)
                                        CurrentParent.mod.distinguish(how="yes", sticky=FirstComment)
                                        CurrentParent.mod.lock()
                                        if FirstComment:
                                            FirstComment = False

                                    # Approve the post, hopefully to avoid being unable to sticky a reddit spam filtered post
                                    submission.mod.approve()
                                    time.sleep(5)
                                    # After waiting then sticky it
                                    submission.mod.sticky(bottom=False)
                                    self.logger.info("Posted to subreddit /r/{}".format(SUBREDDIT))
                                except Exception as err:
                                    self.logger.exception(f"Error posting to DTG subreddit. Err: {err}")

                                for sub in OTHERSUBS:
                                    self.logger.debug("Also posting to /r/{}".format(sub["name"]))
                                    subpost = self.r.subreddit(sub["name"]).submit(
                                        blogtitle,
                                        selftext="Source: " + str(bloglink) + "\n\n---\n\n" + PostText,
                                        send_replies=False,
                                    )
                                    for template in subpost.flair.choices():
                                        if template["flair_text"] == sub["flair"]:
                                            subpost.flair.select(template["flair_template_id"])
                                            break
                            except Exception as err:
                                self.logger.exception(f"Error posting to the subreddits")
                            finally:
                                # ALWAYS Alert us
                                if submission:
                                    postid = submission.id
                                else:
                                    postid = None
                                if NotificationsEnabled:
                                    self.notifier(blog.published, blogtitle, bloglink, postid)

                            # Create imgur Album
                            album = None
                            # TODO - Figure out what sets 'meta' and why it's not being set
                            if self.imgur.Available and meta is not None and meta["properties"]["FrontPageBanner"]:
                                src = "https://www.bungie.net{}".format(meta["properties"]["FrontPageBanner"])
                                try:
                                    resp = self.imgur.imageUpload(
                                        image=src,
                                        imgType="URL",
                                        description="Source: {}".format(src),
                                    )
                                    if resp["success"] and resp["status"] == 200:
                                        img = str(resp["data"]["id"])
                                        resp = self.imgur.albumCreate(
                                            title=meta["properties"]["Title"],
                                            description="Bungie Blog Post: {}\nReddit Post: https://www.reddit.com{}".format(
                                                bloglink, submission.permalink
                                            ),
                                            privacy="public",
                                            cover=img,
                                        )
                                        if resp["success"] and resp["status"] == 200:
                                            album = resp["data"]["id"]
                                            self.imgur.albumAddImgs(album, ids=[img])
                                except RuntimeError as e:
                                    self.logger.exception("Can't upload image to imgur: {}".format(e))
                            if h.imgurUploads:
                                if not album:
                                    self.imgur.albumCreate(
                                        title=blogtitle,
                                        description="Post: {}".format(bloglink),
                                        privacy="public",
                                        ids=h.imgurUploads,
                                    )
                                else:
                                    self.imgur.albumAddImgs(album, ids=h.imgurUploads)

                time.sleep(90)

            except UnicodeEncodeError as e:
                self.logger.error("Caught UnicodeEncodeError! {}".format(str(e)))
            except prawcore.exceptions.InvalidToken:
                self.logger.warning("API Token Error. Likely on reddits end. Issue self-resolves.")
                time.sleep(180)
            except prawcore.exceptions.BadJSON:
                self.logger.warning("PRAW didn't get good JSON, probably reddit sending bad data due to site issues.")
                time.sleep(180)
            except (
                prawcore.exceptions.ResponseException,
                prawcore.exceptions.RequestException,
                prawcore.exceptions.ServerError,
                urllib3.exceptions.TimeoutError,
                Timeout,
            ):
                self.logger.warning("HTTP Requests Error. Likely on reddits end due to site issues.")
                time.sleep(300)
            except praw.exceptions.APIException:
                self.logger.error("PRAW/Reddit API Error")
                time.sleep(30)
            except praw.exceptions.ClientException:
                self.logger.error("PRAW Client Error")
                time.sleep(30)
            except (psycopg2.InterfaceError, psycopg2.OperationalError):
                self.logger.exception("Database error, usually caused by a closed connection. Attempting to reconnect.")
                self.init_database()
                time.sleep(180)
            except KeyboardInterrupt as e:
                self.logger.warning("Caught KeyboardInterrupt - Exiting")
                sys.exit()
            except Exception as err:
                self.logger.exception(f"General Exception - Sleeping 5 min. Error: {err}")
                time.sleep(300)
        else:
            self.logger.warning("Exiting Bot")
            sys.exit()


def main():
    bot = blogbot()
    bot.main()
